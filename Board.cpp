#include <stdlib.h>
#include <time.h>

class Board {
	public:
		static const int SIZE = 4;
		static const int LEFT = 0;
		static const int RIGHT = 1;
		static const int DOWN = 2;
		static const int UP = 3;

		Board();

		int win();
		int lost();
		int over();

		void move(int);

		int get(int, int);
		int getScore();

	private:
		static const int NEW = 2;
		static const int MAX = 2048;

		int board[SIZE][SIZE];
		int score, changed;

		int empty();
		void generate();

		void join(int *, int *);
		void swap(int *, int *);

		int *get(int, int, int);
};

/*** Constructor Funtion ***/
Board::Board() {
	for (int i = 0; i < SIZE; i++)
		for (int j = 0; j < SIZE; j++)
			board[i][j] = 0;

	score = 0;
	changed = 0;

	srand(time(0));

	generate(); generate();
}

/*** Checking Functions ***/
int Board::empty() {
	int r = 0;

	for (int i = 0; i < SIZE; i++)
		for (int j = 0; j < SIZE; j++)
			if (!board[i][j])
				r++;

	return r;
}

int Board::win() {
	for (int i = 0; i < SIZE; i++)
		for (int j = 0; j < SIZE; j++)
			if (board[i][j] == MAX)
				return 1;

	return 0;
}

int Board::lost() {
	if (win() || empty()) return 0;

	for (int i = 0; i < SIZE; i++)
		for (int j = 0; j < SIZE - 1; j++)
			if (board[i][j] == board[i][j + 1])
				return 0;

	for (int i = 0; i < SIZE - 1; i++)
		for (int j = 0; j < SIZE; j++)
			if (board[i][j] == board[i + 1][j])
				return 0;

	return 1;
}

int Board::over() {
	return lost() || win();
}

/*** Get Data Functions ***/
int Board::getScore() {
	return score;
}

int Board::get(int x, int y) {
	return board[x][y];
}

int *Board::get(int i, int j, int v) {
	switch (v) {
		case LEFT:
			return &board[i][j];
		case RIGHT:
			return &board[i][SIZE - 1 - j];
		case DOWN:
			return &board[SIZE - 1 - j][i];
		case UP:
			return &board[j][i];
	}
}

/*** Generate Function ***/
void Board::generate() {
	int n = rand() % empty();

	for (int i = 0; i < SIZE; i++)
		for (int j = 0; j < SIZE; j++)
			if (board[i][j] == 0) {
				if (n) n--;
				else {
					board[i][j] = NEW;
					return;
				}
			}
}

/*** Action Functions ***/
void Board::join(int *a, int *b) {
	if (*a != *b) return;

	score += *a += *b; *b = 0;

	changed = 1;
}

void Board::swap(int *a, int *b) {
	if (a == b) return;

	int r = *a; *a = *b; *b = r;

	changed = 1;
}

void Board::move(int v) {
	if (over()) return;

	for (int i = 0; i < SIZE; i++) {
		int k = SIZE;
		for (int j = 0; j < SIZE; j++)
			if (*get(i, j, v)) {
				if (k < SIZE && *get(i, k, v) == *get(i, j, v)) {
					join(get(i, j, v), get(i, k, v));
					k = SIZE;
				} else k = j;
			}
	}

	for (int i = 0; i < SIZE; i++) {
		int k = 0;
		for (int j = 0; j < SIZE; j++)
			if (*get(i, j, v))
				swap(get(i, j, v), get(i, k++, v));
	}

	if (!over() && changed) generate();
	changed = 0;
}

