#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Board.cpp"
#include "PressAnyKey.cpp"

class Manager {
	private:
		static const int tab = 5;
		Board gboard;
		int max_score;

		char *adjust(int, char *);
		void print();
		void play();
		void updateScore();

	public:
		Manager();

		void main();
};

/*** Constructor Function ***/
Manager::Manager() {
	max_score = 0;
}

/*** Sub-Functions ***/
char *Manager::adjust(int a, char *s) {
	int n = tab - 1;

	for (int i = 0; i < tab; i++) s[i] = ' ';
	s[tab] = '\0';

	while (a > 0) {
		s[n--] = (char) '0' + a % 10;
		a /= 10;
	}

	return s;
}

void Manager::print() {
	int nChar = gboard.SIZE * (tab + 1) + 1;
	char c = '.';
	char s[tab + 1];

	system(CLEAR_SCREEN);

	for (int j = 0; j < nChar; j++) printf("%c", c);
	printf("\n");

	for (int i = 0; i < gboard.SIZE; i++) {
		for (int j = 0; j < gboard.SIZE; j++)
			printf("%c%s", c, adjust(gboard.get(i, j), s));
		printf("%c\n", c);

		for (int j = 0; j < nChar; j++)
			printf("%c", c);
		printf("\n");
	}

	printf("\nScore: %d\nMax Score: %d\n", gboard.getScore(), max_score);
}

void Manager::play() {
	int key = PressAnyKey();

	switch (key) {
		case KEY_LEFT:
			gboard.move(gboard.LEFT); break;
		case KEY_RIGHT:
			gboard.move(gboard.RIGHT); break;
		case KEY_DOWN:
			gboard.move(gboard.DOWN); break;
		case KEY_UP:
			gboard.move(gboard.UP); break;
	}

	updateScore();
}

void Manager::updateScore() {
	if (max_score < gboard.getScore()) max_score = gboard.getScore();
}

/*** Main Function ***/
void Manager::main() {
	while (1) {
		/***** Initializing *****/
		gboard = Board();

		/***** Playing *****/
		while (1) {
			print();

			if (gboard.over()) {
				if (gboard.win()) printf("\n*** YOU WIN! ***\n");
				else printf("\n*** GAME OVER ***\n");
				break;
			}

			play();
		}

		/***** Asking *****/
		char c;

		printf("\nDo you play again?\npress y or n\n");

		do {
			c = PressAnyKey();
			if (c == 'y' || c == 'Y' || c == 'n' || c == 'N')
				break;
		} while (1);

		if (c == 'n' || c == 'N') break;		
	}
}
