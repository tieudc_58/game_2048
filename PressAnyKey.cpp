#include <unistd.h>
#include <termios.h>

#define CLEAR_SCREEN "clear"

#define KEY_UP -65
#define KEY_DOWN -66
#define KEY_RIGHT -67
#define KEY_LEFT -68

int PressAnyKey() {
	#define MAGIC_MAX_CHARS 18

	struct termios initial_settings;
	struct termios settings;
	unsigned char keycodes[MAGIC_MAX_CHARS];
	int count;

	tcgetattr(STDIN_FILENO, &initial_settings);
	settings = initial_settings;

	/* Set the console mode to no-echo, raw input. */
	settings.c_cc[VTIME] = 1;
	settings.c_cc[VMIN] = MAGIC_MAX_CHARS;
	settings.c_iflag &= ~(IXOFF);
	settings.c_lflag &= ~(ECHO | ICANON);

	tcsetattr(STDIN_FILENO, TCSANOW, &settings);

	count = read(STDIN_FILENO, (void*) keycodes, MAGIC_MAX_CHARS);

	tcsetattr(STDIN_FILENO, TCSANOW, &initial_settings);

	return (count == 1) ? keycodes[0] : -(int) (keycodes[count - 1]);
}

